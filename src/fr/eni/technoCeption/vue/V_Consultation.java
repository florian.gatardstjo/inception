/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eni.technoCeption.vue;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import fr.eni.technoCeption.constantes.Constantes;
import fr.eni.technoCeption.controleur.C_AS_AutoSearch;
import fr.eni.technoCeption.db.Db_mysql;
import fr.eni.technoCeption.model.M_Liaison;
import fr.eni.technoCeption.model.M_User;

/**
 *
 * @author gatardf
 */
public class V_Consultation extends javax.swing.JFrame {

    /**
     * Creates new form V_Consultation
     */
    
    //déclaration de la base de donnée et du controlleur
    private C_AS_AutoSearch leControl;
    private Db_mysql bdAutoSearch;
    
    //déclration des dictionnaires
    private ArrayList<M_Liaison> lesMotsClees;
    private ArrayList<M_User> lesUtilisateur;
    private ArrayList<JButton> lesBoutons = new ArrayList<JButton>();;  
    
    //déclaration des objets utilisés
    private M_User unUtilisateur;
    
    
    /**
     * V_Consultation est le constructeur de la classe
     * @param leControl : controlleur
     * @param bdAutoSearch : base de donnée
     */
    public V_Consultation(C_AS_AutoSearch leControl,Db_mysql bdAutoSearch) {
        //Initialisation des attributs importants
        this.bdAutoSearch = bdAutoSearch;
        this.leControl = leControl;
        initComponents();
    }
    
    /**
     * afficher
     * permet l'affichage de la vue consultation
     * @param login : chaine de caractère
     * @throws SQLException 
     */
    public void afficher(String login) throws SQLException{
        //initialisation des variables
        int k =0;
        
        //vérification
        System.out.println(login);
        
        //initialisation de la variable
        unUtilisateur = null;
        
        //déclaration des objet
        unUtilisateur = new M_User(bdAutoSearch, login);
        
        //Si l'utilisateur est un Utilisateur
        if(unUtilisateur.getDroit() ==20){
            
            //initialisation dictionnaire
            lesMotsClees = null;
            
            //récupérationde tous les mots clés du compte
            lesMotsClees = M_Liaison.records(bdAutoSearch, login);
            
            //conte le nombre d'objet dans le dictionnaire
            int i = lesMotsClees.size();
            
            //pour chaque objet faire
            for (int j = 0; j < i; j++) {
                //déclaration et initialisation de l'objet Liaison
                M_Liaison leMotCle;
                leMotCle = lesMotsClees.get(j);
                
                //Déclaration et initialisation du bouton
                JButton bouton;
                bouton = new JButton("bt_moCle"+j);
                
                //donne une position au bouton (décalage gauche, décalage haut, largeur, longueur)
                bouton.setBounds(75,25+k, 200, 20);
                
                //Donne un libelle au bouton
                bouton.setText(leMotCle.getMotCle());
                
                //Défini l'action éxécuter par le bouton lors du clique
                bouton.addActionListener((ActionEvent e) -> {
                    try {
                        //affiche les url pour le mot clé sélectionner
                        leControl.aff_V_Url("Medecine", login, 0,leMotCle.getIdMotCle());
                        
                        //ferme la vue
                        setVisible(false);
                        
                    //gère les erreurs sql
                    } catch (SQLException | ParseException ex) {
                        Logger.getLogger(V_Consultation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                
                //ajout le bouton à la vue
                pan_Bouton.add(bouton);
                               
                //ajout 25 à la position en hauteur
                k=k+25; 
            }
            
        //sinon si l'utilisateur est administrateur
        }else if(unUtilisateur.getDroit() == 1){
            
            //récupère tous les utilisateurs
            lesUtilisateur = M_User.records(bdAutoSearch, unUtilisateur.getDroit());
            
            //compte le nombre d'utilisateur
            int i = lesUtilisateur.size();
            
            //pour chaque utilisateur faire
            for (int j = 0; j < i; j++) {
                
                //Déclaration et initialisation de l'utilisateur
                M_User unUser;
                unUser = lesUtilisateur.get(j);
                
                //Déclaration et initialisation du bouton
                JButton bouton;
                bouton = new JButton("bt_Utilisateur"+j);
                
                //donne une position au bouton (décalage gauche, décalage haut, largeur, longueur)
                bouton.setBounds(75,25+k, 200, 20);
                
                //Donne un libelle au bouton
                bouton.setText(unUser.getLogin());
                
                //Défini l'action éxécuter par le bouton lors du clique
                bouton.addActionListener((ActionEvent e) -> {
                    try {
                        //affiche la vue de l'utilisateur selectinner
                        leControl.aff_V_User(unUser.getLogin(), unUtilisateur.getDroit());
                        
                    //permet de gerer les erreur sql
                    } catch (SQLException | ParseException ex) {
                        Logger.getLogger(V_Consultation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //fait disparaitre la vue
                    setVisible(false);
                });
                
                //ajout le bouton
                pan_Bouton.add(bouton);
                
                //ajout 25 de hauteur au bouton
                k=k+25; 
            }
        //sinon
        }else{
            //récupère tous les utilisateurs avec un rang inférieur au sien
            lesUtilisateur = M_User.records(bdAutoSearch, unUtilisateur.getDroit());
            
            //compte le nombre d'utilisateur
            int i = lesUtilisateur.size();
            
            //pour chaque tuilisateur faire
            for (int j = 0; j < i; j++) {
                //déclaration et initialisation de 'lutilisateur
                M_User unUser;
                unUser = lesUtilisateur.get(j);
                
                //déclaration et intialisation du bouton
                JButton bouton;
                bouton = new JButton("bt_Utilisateur"+j);
                
                //donne une position au bouton (décalage gauche, décalage haut, largeur, longueur)
                bouton.setBounds(75,25+k, 200, 20);
                
                //donne un libelle au botuon
                bouton.setText(unUser.getLogin());
                
                //Défini l'action éxécuter par le bouton lors du clique
                bouton.addActionListener((ActionEvent e) -> {
                    try {
                        
                        //permet d'afficher la vue de l'utilisateur
                        leControl.aff_V_User(unUser.getLogin(), unUtilisateur.getDroit());
                        
                    //permet de gerer les erreurs sql
                    } catch (SQLException | ParseException ex) {
                        Logger.getLogger(V_Consultation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    //masque la fenetre
                    setVisible(false);
                });            
                //ajout le bouton à la vue
                pan_Bouton.add(bouton);
                
                //ajout 25 à la position du bouton
                k=k+25; 
            }
        }
        
        //affiche la vue
        setVisible(true);
    }
     
    /**
     * permet d'afficher la vue
     */
    public void afficher(){
        
        //affiche la vue
        setVisible(true);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bt_retour = new javax.swing.JButton();
        pan_Bouton = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Auto search");
        setMaximumSize(new java.awt.Dimension(350, 300));
        setMinimumSize(new java.awt.Dimension(350, 300));
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        setPreferredSize(new java.awt.Dimension(350, 300));

        bt_retour.setIcon(new javax.swing.ImageIcon(getClass().getResource(Constantes.ICON_DECONNEXION))); // NOI18N
        bt_retour.setBorder(null);
        bt_retour.setBorderPainted(false);
        bt_retour.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt_retour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_retourActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pan_BoutonLayout = new javax.swing.GroupLayout(pan_Bouton);
        pan_Bouton.setLayout(pan_BoutonLayout);
        pan_BoutonLayout.setHorizontalGroup(
            pan_BoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pan_BoutonLayout.setVerticalGroup(
            pan_BoutonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 318, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bt_retour)
                .addGap(0, 465, Short.MAX_VALUE))
            .addComponent(pan_Bouton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bt_retour)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pan_Bouton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * permet la deconnexion
     * @param evt 
     */
    private void bt_retourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_retourActionPerformed
        //affiche le vue principale
        leControl.aff_V_Main();
        
        //Detruit tous les éléments du pannel
        pan_Bouton.removeAll();
        
        //ferme la vue
        setVisible(false);
    }//GEN-LAST:event_bt_retourActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(V_Consultation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(V_Consultation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(V_Consultation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(V_Consultation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new V_Consultation(null,null).setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bt_retour;
    private javax.swing.JPanel pan_Bouton;
    // End of variables declaration//GEN-END:variables
}
