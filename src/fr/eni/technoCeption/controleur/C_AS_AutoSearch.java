/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eni.technoCeption.controleur;

//Bibliothèque importé
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.eni.technoCeption.db.Cl_Connect;
import fr.eni.technoCeption.db.Db_mysql;
import fr.eni.technoCeption.vue.V_Consultation;
import fr.eni.technoCeption.vue.V_Main;
import fr.eni.technoCeption.vue.V_Modifier;
import fr.eni.technoCeption.vue.V_Url;
import fr.eni.technoCeption.vue.V_User;

/**
 * Controlleur du projet C_AS_AutoSearch
 * @author gatardf
 */
public class C_AS_AutoSearch {

    //Déclaration des attributs de la classe
        //Déclaration des Vues
        private V_Main frm_Main;
        private V_Consultation frm_Consultation;
        private V_Url frm_Url;
        private V_Modifier frm_Modifier;
        private V_User frm_User;
        
        //Déclaration de la DataBase
        private Db_mysql bdAutoSearch;
    
    /**
     * Constructeur du controlleur
     * @param aucun
     * @throws IOException
     * @throws ParseException
     * @throws Exception 
     */
    public C_AS_AutoSearch() throws IOException, ParseException, Exception{
        //connexion à la base de donnée
        this.connection();
        
        //Création de toutes les vues de l'application
        frm_Main = new V_Main(this, bdAutoSearch);
        frm_Consultation = new V_Consultation(this, bdAutoSearch);
        frm_Url = new V_Url(this, bdAutoSearch);
        frm_Modifier = new V_Modifier(this, bdAutoSearch);
        frm_User = new V_User(this, bdAutoSearch);
        
        //affichage de la vue principale
        frm_Main.afficher();
    }
    
    /**
     * aff_V_Main
     * permet l'affichage de la vue main
     * @param aucun
     */
    public void aff_V_Main(){
        //affichage de la vue principale
        frm_Main.afficher("retour");
    }
    
    /**
     * connection
     * permet de ce connecter à la base de données
     * @param aucun
     * @throws Exception 
     */
    private void connection() throws Exception{
        //Création d'une nouvelle data base
        bdAutoSearch = new Db_mysql(Cl_Connect.url, Cl_Connect.login, Cl_Connect.password);
    }
    
    /**
     * aff_V_Consultation
     * permet l'affichage de la vue Consultation
     * @param login : Chaine de caractère
     * @throws IOException
     * @throws ParseException
     * @throws SQLException 
     */
    public void aff_V_Consultation(String login) throws IOException, ParseException, SQLException{
        //affichage de la vue Consulation
        frm_Consultation.afficher(login);
    }
    
    /**
     * aff_V_Consultation
     * permet l'affichage de la vue Consultation
     * @param aucun
     * @throws IOException
     * @throws ParseException
     * @throws SQLException 
     */
    public void aff_V_Consultation() throws IOException, ParseException, SQLException{
        //initialisation
        frm_Consultation = null;
        
        //affichage de la vue Consulation
        frm_Consultation.afficher();
    }
    
    /**
     * aff_V_Url
     * permet l'affichage de la vue Url
     * @param Categorie : chaine de caractère
     * @param login : chaine de caractère
     * @param mois : entier
     * @throws SQLException 
     * @throws java.text.ParseException 
     */
    public void aff_V_Url(String Categorie, String login, int mois, int idMotCle) throws SQLException, ParseException{
        //affichage de la vue Url en passant la catégorie
        frm_Url.afficher(Categorie, login, mois, idMotCle);
    }
    
    public void aff_V_Modifier(int id) throws SQLException, ParseException{
        //affichage de la vue Url en passant la catégorie
        frm_Modifier.afficher(id);
    }
    
    public void aff_V_User(String Login, int droit) throws SQLException, ParseException{
        //affichage de la vue Url en passant la catégorie
        frm_User.afficher(Login, droit);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        try {
            // TODO code application logic here
            C_AS_AutoSearch leControleur=new C_AS_AutoSearch();
        } catch (ParseException ex) {
            Logger.getLogger(C_AS_AutoSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
