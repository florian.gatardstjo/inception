/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eni.technoCeption.model;

//Bibliothèque importé
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

import fr.eni.technoCeption.db.Cl_Connect;
import fr.eni.technoCeption.db.Db_mysql;

/**
 * Modèle de la table liaison
 * @author gatardf
 */
public class M_Liaison {
    
    //Déclaration des attributs de la classe
        //Déclaration des variables
        private int id_Mot;
        private String login;
        private String motCle;
        
        //Déclaration de la DataBase
        private Db_mysql bdAutoSearch;

        
    /**
     * Constructeur de la classe
     * @param bdAutoSearch : DataBase
     * @param Login : Chaine de caractère
     * @param idMot : entier
     * @throws SQLException 
     */
    public M_Liaison(Db_mysql bdAutoSearch,String Login, int idMot) throws SQLException {
        //affectation des attributs
        this.bdAutoSearch = bdAutoSearch;
        this.login = Login;
        this.id_Mot = idMot;
        
        //déclaration des vriables
        String sql;
        ResultSet res;
        
        //affectation des variables
        sql =   "SELECT * FROM IR_AVOIR "+
                "INNER JOIN IR_CLASSIFICATION ON IR_CLASSIFICATION.id = IR_AVOIR.id_Mot "+
                "WHERE login = '"+Login+"' "+
                "AND id_Mot = "+idMot;
        res = bdAutoSearch.sqlSelect(sql);
        
        //récupération du premier resultat de la requette
        res.first();
        
        //récupération du mot clé
        motCle = res.getString("motCle");
    }
    
    //=========== ACCESSEUR DE LA CLASSE =============
    public Db_mysql getBdAnnonce(){
        return bdAutoSearch;
    }
    
    public void setBdAnnonce(Db_mysql bdAutoSearch){
        this.bdAutoSearch = bdAutoSearch;
    }
    
    public String getLogin(){
        return login;
    }
    
    public Integer getIdMotCle(){
        return id_Mot;
    }
    
    public String getMotCle(){
        return motCle;
    }
    
    //=========== METHODE DE LA CLASSE =============
    /**
     * delete
     * peremet de supprimer un enregistrement
     * @throws SQLException 
     */
    public void delete () throws SQLException{
        //déclaration de la variable
        String sql;
        
        //affectation de la variable
        sql = "DELETE FROM AM_ANNONCE WHERE login = '"+login+"';";
        
        //execution de la requette
        bdAutoSearch.sqlExec(sql);
    }
    
    public void insert (String login, int id) throws SQLException{
        //déclaration de la variable
        String sql;
        
        //affectation de la variable
        sql = "INSERT INTO `IR_VISUALISER` (`Login`, `id`) VALUES ('"+login+"', '"+id+"');";
        
        //execution de la requette
        bdAutoSearch.sqlExec(sql);
    }
    
    /**
     * records
     * permet de retourner toutes les mots du compte
     * @param db : database
     * @param sqlWhere : condition de la requette sql
     * @return Collection de M_Liaison
     * @throws SQLException 
     */
    public static ArrayList<M_Liaison> records (Db_mysql db, String sqlWhere) throws SQLException{
        //déclaration des variables
        String sql;
        ResultSet res;
        M_Liaison leMotsClees;
        
        //déclaration et création du dictionnaire
        ArrayList<M_Liaison> lesMotsClees = new ArrayList<M_Liaison>();
        
        //affectation des variables
        sql =   "SELECT * FROM IR_AVOIR "+
                "INNER JOIN IR_CLASSIFICATION ON IR_CLASSIFICATION.id = IR_AVOIR.id_Mot "+
                "WHERE login = '"+sqlWhere+"'";
        res = db.sqlSelect(sql);
        
        //Tant qu'il y a des enregistrements faire
        while (res.next()){
            //création d'un nouveau mot clé
            leMotsClees = new M_Liaison (db,res.getString("login"),res.getInt("id_Mot"));
            
            //ajout à la collection
            lesMotsClees.add(leMotsClees);
        }
        
        //retourne la collection
        return lesMotsClees;
    }
  
    public static void main(String[] args) throws ParseException, IOException, SQLException, Exception{
        Scanner sc = new Scanner(System.in);
        M_Liaison uneAnnonce;
        ArrayList<M_Liaison> lesAnnonces;
        Db_mysql bdAutoSearch = new Db_mysql(Cl_Connect.url, Cl_Connect.login, Cl_Connect.password);        
    };
}
