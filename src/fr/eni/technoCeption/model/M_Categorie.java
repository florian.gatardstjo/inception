/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eni.technoCeption.model;

//Bibliothèque importé
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import fr.eni.technoCeption.db.Db_mysql;

/**
 * Modèle de la table Categorie
 * @author gatardf
 */
public class M_Categorie {
    
    //Déclaration des attributs de la classe
        //Déclaration des variables
        private int idCategorie;
        private String nomCategorie;

        //Déclaration de la DataBase
        private Db_mysql bdAutoSearch;
    
    /**
     * Constructeur de la classe
     * @param bdAutoSearch : database
     * @param idCategorie : entier
     * @throws SQLException 
     */
    public M_Categorie(Db_mysql bdAutoSearch,int idCategorie) throws SQLException {
        //affectation des attributs
        this.bdAutoSearch = bdAutoSearch;
        this.idCategorie = idCategorie;
        
        //Déclaration des variables
        String sql;
        ResultSet res;
        
        //affectation des variables
        sql = "SELECT * FROM IR_CATEGORIE WHERE idCategorie = "+idCategorie;
        res = bdAutoSearch.sqlSelect(sql);
        
        //récupère le premier enregistrement de la requète
        res.first();
        
        //récupère le libelle de la categorie
        nomCategorie = res.getString("libelle");
    }
    
    //=========== ACCESSEUR DE LA CLASSE =============
    public String getLibelle(){
        return nomCategorie;
    }
    
    /**
     * records
     * permet de retourner toutes les categories
     * @param db : dataBase
     * @return ArrayList<M_Categorie>
     * @throws SQLException 
     */
    public static ArrayList<M_Categorie> records (Db_mysql db) throws SQLException{
        //Initialisation de la bibliothèque
        ArrayList<M_Categorie> lesCategories = new ArrayList<M_Categorie>();
        
        //Déclaration des variables
        String sql;
        ResultSet res;
        M_Categorie laCategorie;
        
        //Déclaration et éxecution de la requete
        sql = "SELECT * FROM IR_CATEGORIE";
        res = db.sqlSelect(sql);
        
        //parcour du tableau retourner par la requete
        while (res.next()){
            //Initialisation de la categorie
            laCategorie = new M_Categorie(db,res.getInt("idCategorie"));
            
            //ajout de la categorie au dictionnaire
            lesCategories.add(laCategorie);
        }
        
        //retourne le dictionnaire
        return lesCategories;
    }
}
