/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eni.technoCeption.model;

//bibliothèque importé
import java.util.Date;

import fr.eni.technoCeption.db.Db_mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;

/**
 * Modèle de la table IR_URL
 * @author gatardf
 */
public class M_Url {
    
    //Déclaration des attributs de la classe
        //Déclaration des variables
        private int id;
        private String url;
        private String titre;
        private Date date;
        private Time heure;
        
        //Déclaration de la DataBase
        private Db_mysql bdAutoSearch;
    
        
    /**
     * Constructeur de la classe
     * @param bdAutoSearch : DataBase
     * @param id : entier
     * @throws SQLException 
     */
    public M_Url(Db_mysql bdAutoSearch,int id, int idCategorie) throws SQLException {
        //affectation des attributs
        this.bdAutoSearch = bdAutoSearch;
        this.id = id;
        
        //Déclaration des varaibles
        String sql;
        ResultSet res;
        
        //affectation des varaibles
        sql = "SELECT * FROM IR_URL INNER JOIN IR_LIAISON ON IR_URL.id=IR_LIAISON.idUrl WHERE id = "+id+" AND idCategorie = "+idCategorie;
        res = bdAutoSearch.sqlSelect(sql);
        
        //première valeur de la requette
        res.first();

        //recupération de l'url et du titre
        url = res.getString("url");
        titre = res.getString("titre").replaceAll("&#039;", "'").replaceAll("&rsquo;", "'").replaceAll("intelligence artificielle", "IA").replaceAll("Intelligence artificielle", "IA");
        date = res.getDate("dateAjout");
        heure = res.getTime("dateAjout");
    }
    
    public M_Url(Db_mysql bdAutoSearch,int id) throws SQLException {
        //affectation des attributs
        this.bdAutoSearch = bdAutoSearch;
        this.id = id;
        
        //Déclaration des varaibles
        String sql;
        ResultSet res;
        
        //affectation des varaibles
        sql = "SELECT * FROM IR_URL WHERE id = "+id+" ";
        res = bdAutoSearch.sqlSelect(sql);
        
        //première valeur de la requette
        res.first();

        //recupération de l'url et du titre
        url = res.getString("url");
        titre = res.getString("titre").replaceAll("&#039;", "'").replaceAll("&rsquo;", "'").replaceAll("intelligence artificielle", "IA").replaceAll("Intelligence artificielle", "IA");
    }
    
    public M_Url(Db_mysql bdAutoSearch,String login, int id) throws SQLException{
        //déclaration de la variable
        String sql;
        
        //affectation de la variable
        sql = "INSERT INTO IR_LIAISON  VALUES ("+id+", 8, DEFAULT);";
        
        //execution de la requette
        bdAutoSearch.sqlExec(sql);
    }
    
    //=========== ACCESSEUR DE LA CLASSE =============
    public String getUrl(){
        return url;
    }
    
    public String getTitre(){
        return titre;
    }
    
    public int getId(){
        return id;
    }

    public Date getDate() {
        return date;
    }
    
    public Time getHeure() {
        return heure;
    }
    
    
    //=========== METHODE DE LA CLASSE =============    
    public static ArrayList<M_Url> records (Db_mysql db,String sqlWhere, int mois, int idMotCle) throws SQLException{
        
        //Déclaration des variables
        String sql;
        ResultSet res;
        M_Url lUrl;
        
        //Déclaration et initialisation du dictionnaire
        ArrayList<M_Url> lesUrl = new ArrayList<M_Url>();

        //Si le mois vaux 0
        if(mois == 0){
            
            //Préparation de la requete sans traitement sur les mois
            sql = " SELECT IR_URL.id, IR_CATEGORIE.idCategorie, IR_CLASSIFICATION.id FROM `IR_LIAISON` "+
                  " INNER JOIN IR_CATEGORIE ON IR_CATEGORIE.idCategorie = IR_LIAISON.idCategorie "+
                  " INNER JOIN IR_URL ON IR_URL.id = IR_LIAISON.idUrl "+
                  " INNER JOIN IR_APPARTENIR ON IR_APPARTENIR.id_Url=IR_URL.id "+
                  " INNER JOIN IR_CLASSIFICATION ON IR_CLASSIFICATION.id=IR_APPARTENIR.id_Classification "+
                  " WHERE IR_CATEGORIE.libelle = '"+sqlWhere+"' "+
                  " AND IR_CLASSIFICATION.id="+idMotCle+" "+
                  " ORDER BY dateAjout DESC";
        //Sinon
        }else{
            
            //Préparation de la requete avec les traitements sur les dates
            sql = " SELECT IR_URL.id, IR_CATEGORIE.idCategorie, IR_CLASSIFICATION.id FROM `IR_LIAISON` "+
                  " INNER JOIN IR_CATEGORIE ON IR_CATEGORIE.idCategorie = IR_LIAISON.idCategorie "+
                  " INNER JOIN IR_URL ON IR_URL.id = IR_LIAISON.idUrl "+
                  " INNER JOIN IR_APPARTENIR ON IR_APPARTENIR.id_Url=IR_URL.id "+
                  " INNER JOIN IR_CLASSIFICATION ON IR_CLASSIFICATION.id=IR_APPARTENIR.id_Classification "+
                  " WHERE IR_CATEGORIE.libelle = '"+sqlWhere+"' "+
                  " AND IR_CLASSIFICATION.id="+idMotCle+" "+
                  " AND MONTH(`dateAjout`) = "+mois+
                  " ORDER BY dateAjout DESC";
        }
        
        //éxécution de la requete
        res = db.sqlSelect(sql);
        
        //parcour du resultat de la requete
        while (res.next()){
            //Initialisation de l'Url
            lUrl = new M_Url(db,res.getInt("id"), res.getInt("idCategorie"));
            
            //Ajout de l'url à la categorie
            lesUrl.add(lUrl);
        }
        
        //Retourne le dictionnaire
        return lesUrl;
    }
    
        public static void Sup_Url(Db_mysql bdAutoSearch, int id) throws SQLException{
            //déclaration de la variable
            String sql;

            //affectation de la variable
            sql = "DELETE FROM IR_LIAISON WHERE idUrl="+id+" AND idCategorie = 8";

            //execution de la requette
            bdAutoSearch.sqlExec(sql);
        }
        
        public static void Mod_Titre(Db_mysql bdAutoSearch, int id, String Titre) throws SQLException{
            //déclaration de la variable
            String sql;

            //affectation de la variable
            sql = "UPDATE IR_URL SET titre='"+Titre+"' WHERE id="+id+";";

            //execution de la requette
            bdAutoSearch.sqlExec(sql);
        }
        
        public static boolean visiter(Db_mysql db, int id) throws SQLException{
            //Déclaration et initialisation de la variable
            int valeur = 0;
            String sql;
            
            //affectation et éxecution de la variable
            sql = "SELECT idUrl FROM IR_LIAISON WHERE idUrl = "+id;
            ResultSet res = db.sqlSelect(sql);
            
            //parcour du resultat de la requette
            while (res.next()){
                //ajoute un a la valeur
                valeur ++;
            }
            
            //retourne vrai si il y a deux résultats
            return valeur == 2;
        }
}
