/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eni.technoCeption.model;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

import fr.eni.technoCeption.db.Cl_Connect;
import fr.eni.technoCeption.db.Db_mysql;
import fr.eni.technoCeption.hash.BCrypt;

/**
 *
 * @author gatardf
 */
public class M_User {
    //Déclaration des attributs privées
    private Db_mysql bdAutoSearch;
    private String login;
    private String password;
    private String nom;
    private String prenom;
    private String mail;
    private String phone;
    private int droit;

    /**
     * M_User
     * Controlleur
     * @param bdAutoSearch : base de données
     * @param login : chaine de caractère
     * @throws SQLException 
     */
    public M_User(Db_mysql bdAutoSearch, String login) throws SQLException {
        //Déclaration et intialisation des attributs privées
        this.bdAutoSearch = bdAutoSearch;
        this.login = login;
        
        //Initialisation et éxecution de la requete
        String sql = "SELECT * FROM IR_COMPTE WHERE login = '"+this.login+"'";
        ResultSet res = bdAutoSearch.sqlSelect(sql);
        
        //Parcour du premier resultat retourner par la requete si il existe
        if(res.first()){
            res.first();
            
            //Initialisation de tous les attributs non initialiser
            password = res.getString("password");
            nom = res.getString("nom");
            prenom = res.getString("prenom");
            mail = res.getString("mail");
            phone = res.getString("phone");
            droit = res.getInt("idDroit");
            
        //sinon
        }else{
            //donne la valeur rien au mot de passe
            password = "rien";
        }
    }
    
    //==============ACESSEURS==================
    public Db_mysql getBdAnnonce(){
        return bdAutoSearch;
    }
    
    public void setBdAnnonce(Db_mysql bdAutoSearch){
        this.bdAutoSearch = bdAutoSearch;
    }
    
    public String getLogin(){
        return login;
    };
    
    public String getPassword(){
        return password;
    };
    
    public String getNom(){
        return nom;
    };
    
    public String getPrenom(){
        return prenom;
    };
    
    public String getMail(){
        return mail;
    };
    
    public String getPhone(){
        return phone;
    };

    public int getDroit() {
        return droit;
    }
    
    public void setPassword(String password){
        this.password = BCrypt.hashpw(password, BCrypt.gensalt());
    };
    
    public void setPhone(String phone){
        this.phone = phone;
    };
    
    //=============METHODE=============
    /**
     * update
     * permet de modifier un compte
     * @param bd : base de donnée
     * @param login : chaine de caractère
     * @param mail : chaine de caractère
     * @param Nom : chaine de caractère
     * @param Prenom : chaine de caractère
     * @param phone : chaine de caractère
     * @throws SQLException 
     */
    public static void update (Db_mysql bd, String login, String mail, String Nom, String Prenom, String phone) throws SQLException{
        //Déclaration de la variable
        String sql;
        
        //Initialisation et éxecution de la requete
        sql = "UPDATE IR_COMPTE SET mail = '"+mail+"', nom = '"+Nom+"', prenom = '"+Prenom+"', phone = '"+phone+"' WHERE login = '"+login+"';";
        bd.sqlExec(sql);
    }
    
    /**
     * delete
     * permet de supprimer un utilisateur
     * @param bd : base de données
     * @param login : chaine de caractère
     * @throws SQLException 
     */
    public static void delete (Db_mysql bd, String login) throws SQLException{
        //Déclaration de la variable
        String sql;
        
        //Initialisation et éxécution de la requete
        sql = "DELETE FROM IR_COMPTE WHERE login = '"+login+"';";
        bd.sqlExec(sql);
    }
    
    /**
     * 
     * @param db : base de données
     * @param droit : entier
     * @return dictionnaire
     * @throws SQLException 
     */
    public static ArrayList<M_User> records (Db_mysql db, int droit) throws SQLException{
        //déclaration des varaible
        String sql;
        ResultSet res;
        M_User unUser;
        
        //Initialisation du dictionnaire
        ArrayList<M_User> lesUser = new ArrayList<M_User>();

        //Initialisation et éxecution de la requete
        sql = " SELECT * FROM IR_COMPTE WHERE idDroit > "+droit;
        res = db.sqlSelect(sql);
        
        //parcour le resultat de la requete
        while (res.next()){
            //Initialiation de l'utilisateur
            unUser = new M_User(db,res.getString("login"));
            
            //Ajout l'utilisateur au dictinnaire
            lesUser.add(unUser);
        }
        
        //retourne le dictionnaire
        return lesUser;
    }
    
    
    public static void main(String[] args) throws ParseException, IOException, SQLException, Exception{
        Scanner sc = new Scanner(System.in);
        M_User uneAnnonce;
        ArrayList<M_User> lesAnnonces;
        Db_mysql bdAutoSearch = new Db_mysql(Cl_Connect.url, Cl_Connect.login, Cl_Connect.password);        
    };
}
